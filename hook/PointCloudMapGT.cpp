﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file PointCloudMapGT.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "PointCloudMapGT.h"

using namespace std;

///////////

// Add robot position
void PointCloudMapGT::addPose(const Pose2D &p) { poses.emplace_back(p); }

// Find the representative point of each cell in the grid table and store it in sps
void PointCloudMapGT::subsamplePoints(vector<LPoint2D> &sps)
{
    nntab.clear(); // Initializing the grid table
    for (size_t i = 0; i < allLps.size(); i++)
        nntab.addPoint(&(allLps[i])); // Register all points in grid table

    nntab.makeCellPoints(nthre, sps); // Get representative points from cells with nthre points or more
    // For confirmation
    printf("allLps.size=%lu, sps.size=%lu\n", allLps.size(), sps.size());
}

/////////

// Add scan point cloud
void PointCloudMapGT::addPoints(const vector<LPoint2D> &lps)
{
    for (size_t i = 0; i < lps.size(); i++)
        allLps.emplace_back(lps[i]);
}

// Generation of global map
void PointCloudMapGT::makeGlobalMap()
{
    globalMap.clear();
    subsamplePoints(globalMap); // Create an overall map from the representative points of each cell in the grid table

    printf("GT: globalMap.size=%lu\n", globalMap.size()); // For confirmation
}

// Generating local maps. Use the overall map as is
void PointCloudMapGT::makeLocalMap()
{
    localMap = globalMap;
    printf("GT: localMap.size=%lu\n", localMap.size());
}

////////

// dummy
void PointCloudMapGT::remakeMaps(const vector<Pose2D> &newPoses) {}
