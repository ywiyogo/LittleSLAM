﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file DataAssociatorGT.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "DataAssociatorGT.h"
#include <boost/timer.hpp>

using namespace std;

// Find the point closest to the coordinate transformation of each scan point of the current scan curScan using predPose
double DataAssociatorGT::findCorrespondence(const Scan2D *curScan,
                                            const Pose2D &predPose)
{
    boost::timer tim; // For processing time measurement

    curLps.clear(); // Empty the mapping current scan point cloud
    refLps.clear(); // Empty the mapping reference scan point cloud

    for (size_t i = 0; i < curScan->lps.size(); i++)
    {
        const LPoint2D *clp = &(curScan->lps[i]); // Current scan point. with a pointer.

        // Find the nearest neighbor point using a lattice table. Note that there is a distance threshold dthre in the grid table.
        const LPoint2D *rlp = nntab.findClosestPoint(clp, predPose);

        if (rlp != nullptr)
        {
            curLps.push_back(clp); // Register if there is a nearest neighbor point
            refLps.push_back(rlp);
        }
    }

    double ratio =
        (1.0 * curLps.size()) / curScan->lps.size(); // Ratio of matched points

    //  double t1 = 1000*tim.elapsed();                   // processing time
    //  printf("Elapsed time: dassGT=%g\n", t1);

    return (ratio);
}
