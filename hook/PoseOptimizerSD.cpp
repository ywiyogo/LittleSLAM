﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file PoseOptimizerSD.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "PoseOptimizerSD.h"

using namespace std;

////////

// Under the fixed data correspondence, give the initial value initPose and calculate the estimated robot position estPose
double PoseOptimizerSD::optimizePose(Pose2D &initPose, Pose2D &estPose)
{
    double th = initPose.th;
    double tx = initPose.tx;
    double ty = initPose.ty;
    double txmin = tx, tymin = ty, thmin = th; // Minimum cost solution
    double evmin = HUGE_VAL;                   // Minimum cost
    double evold = evmin;                      // Previous cost value. Used for convergence judgment

    double ev = cfunc->calValue(tx, ty, th); // cost calculation
    int nn = 0;                              // Number of repetitions. For confirmation
    double kk = 0.00001;                     // Steepest descent step width factor
    while (abs(evold - ev) >
           evthre)
    { // Convergence judgment. Terminates if the change from the previous value is small
        nn++;
        evold = ev;

        // Partial differentiation by numerical calculation
        double dEtx = (cfunc->calValue(tx + dd, ty, th) - ev) / dd;
        double dEty = (cfunc->calValue(tx, ty + dd, th) - ev) / dd;
        double dEth = (cfunc->calValue(tx, ty, th + da) - ev) / da;

        // Multiply the differential coefficient by kk to get the step width
        double dx = -kk * dEtx;
        double dy = -kk * dEty;
        double dth = -kk * dEth;
        tx += dx;
        ty += dy;
        th += dth; // Add step width to determine next search position

        ev = cfunc->calValue(tx, ty, th); // Calculate cost at that location

        if (ev < evmin)
        { // Update if ev is the minimum so far
            evmin = ev;
            txmin = tx;
            tymin = ty;
            thmin = th;
        }

        //    printf("nn=%d, ev=%g, evold=%g, abs(evold-ev)=%g\n", nn, ev, evold,
        //    abs(evold-ev));         // For confirmation
    }

    ++allN;
    if (allN > 0 && evmin < 100)
        sum += evmin;
    //  printf("allN=%d, evmin=%g, avg=%g\n", allN, evmin, (sum/allN));         //
    //  For confirmation

    //  printf("nn=%d, ev=%g\n", nn, ev);         // For confirmation

    estPose.setVal(txmin, tymin, thmin); // Save the solution that gives the minimum value

    return (evmin);
}
