﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file CostFunctionED.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "CostFunctionED.h"

using namespace std;

// ICP cost function by point distance
double CostFunctionED::calValue(double tx, double ty, double th)
{
    double a = DEG2RAD(th);
    double error = 0;
    int pn = 0;
    int nn = 0;
    for (size_t i = 0; i < curLps.size(); i++)
    {
        const LPoint2D *clp = curLps[i]; // Current scan point
        const LPoint2D *rlp = refLps[i]; // Reference scan point corresponding to clp

        double cx = clp->x;
        double cy = clp->y;
        double x =
            cos(a) * cx - sin(a) * cy + tx; // Convert clp to reference scan coordinate system
        double y = sin(a) * cx + cos(a) * cy + ty;

        double edis =
            (x - rlp->x) * (x - rlp->x) + (y - rlp->y) * (y - rlp->y); // distance between points

        if (edis <= evlimit * evlimit)
            ++pn; // Number of points with small error

        error += edis; // Accumulate error at each point

        ++nn;
    }

    error = (nn > 0) ? error / nn
                     : HUGE_VAL; // Take the average. If the valid score is 0, the value is HUGE_VAL
    pnrate = 1.0 * pn / nn;      // Ratio of points with small error

    //  printf("CostFunctionED: error=%g, pnrate=%g, evlimit=%g\n", error, pnrate,
    //  evlimit);     // 確認用

    error *= 100; // Multiply by 100 to prevent the evaluation value from becoming too small.

    return (error);
}
