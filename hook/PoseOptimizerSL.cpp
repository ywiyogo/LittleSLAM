﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file PoseOptimizerSL.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "PoseOptimizerSL.h"
#include <boost/math/tools/minima.hpp>

using namespace std;

////////

// Under the fixed data correspondence, give the initial value initPose and calculate the estimated robot position estPose
double PoseOptimizerSL::optimizePose(Pose2D &initPose, Pose2D &estPose)
{
    double th = initPose.th;
    double tx = initPose.tx;
    double ty = initPose.ty;
    double txmin = tx, tymin = ty, thmin = th; // Minimum cost solution
    double evmin = HUGE_VAL;                   // Minimum cost
    double evold = evmin;                      // Previous cost value. Used for convergence judgment
    Pose2D pose, dir;

    double ev = cfunc->calValue(tx, ty, th); // cost calculation
    int nn = 0;                              // Number of repetitions. For confirmation
    while (abs(evold - ev) > evthre)
    { // Convergence judgment. Terminates when the change in value is small
        nn++;
        evold = ev;

        // Partial differentiation by numerical calculation
        double dx = (cfunc->calValue(tx + dd, ty, th) - ev) / dd;
        double dy = (cfunc->calValue(tx, ty + dd, th) - ev) / dd;
        double dth = (cfunc->calValue(tx, ty, th + da) - ev) / da;
        tx += dx;
        ty += dy;
        th += dth; // Decide on the next search position

        // Straight line search using Brent method
        pose.tx = tx;
        pose.ty = ty;
        pose.th = th; // Search starting point
        dir.tx = dx;
        dir.ty = dy;
        dir.th = dth;          // Search direction
        search(ev, pose, dir); // Line search execution
        tx = pose.tx;
        ty = pose.ty;
        th = pose.th; // Position found by straight line search

        ev = cfunc->calValue(tx, ty, th); // Calculate the cost based on the determined location

        if (ev < evmin)
        { // Update if cost is lowest so far
            evmin = ev;
            txmin = tx;
            tymin = ty;
            thmin = th;
        }

        //    printf("nn=%d, ev=%g, evold=%g, abs(evold-ev)=%g\n", nn, ev, evold,
        //    abs(evold-ev));         // 確認用
    }
    ++allN;
    if (allN > 0 && evmin < 100)
        sum += evmin;
    //  printf("allN=%d, nn=%d, evmin=%g, avg=%g, evthre=%g\n", allN, nn, evmin,
    //  (sum/allN), evthre);         // 確認用

    //  printf("nn=%d, evmin=%g\n", nn, evmin);       // 確認用

    estPose.setVal(txmin, tymin, thmin); // Save the solution that gives the minimum value

    return (evmin);
}

////////// Line search ///////////

// Perform a line search using the boost library's Brent method.
// Starting from pose, find the step width to determine how far to advance in the dp direction.
double PoseOptimizerSL::search(double ev0, Pose2D &pose, Pose2D &dp)
{
    int bits = numeric_limits<double>::digits; // Search accuracy
    boost::uintmax_t maxIter = 40;             // Maximum number of repetitions. decide empirically
    pair<double, double> result = boost::math::tools::brent_find_minima(
        [this, &pose, &dp](double tt)
        { return (objFunc(tt, pose, dp)); },
        -2.0,
        2.0, bits, maxIter); // Search range (-2.0,2.0)

    double t = result.first;  // Desired step width
    double v = result.second; // Minimum value to seek

    pose.tx = pose.tx + t * dp.tx; // Store the required minimum solution in pose
    pose.ty = pose.ty + t * dp.ty;
    pose.th = MyUtil::add(pose.th, t * dp.th);

    return (v);
}

// Objective function for line search. tt is step width
double PoseOptimizerSL::objFunc(double tt, Pose2D &pose, Pose2D &dp)
{
    double tx = pose.tx + tt * dp.tx; // Move forward by tt in the dp direction from pose
    double ty = pose.ty + tt * dp.ty;
    double th = MyUtil::add(pose.th, tt * dp.th);
    double v = cfunc->calValue(tx, ty, th); // cost function value

    return (v);
}
