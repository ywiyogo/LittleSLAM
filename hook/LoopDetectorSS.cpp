﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file LoopDetectorSS.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "LoopDetectorSS.h"

using namespace std;

////////////

// loop detection
// Find a place from the robot trajectory that is close to the current position curPose and whose shape matches the current scan curScan, and create a pose arc.
bool LoopDetectorSS::detectLoop(Scan2D *curScan, Pose2D &curPose, int cnt)
{
    printf("-- detectLoop -- \n");

    // Find the nearest partial map
    double atd = pcmap->atd;                        // Current actual cumulative mileage
    double atdR = 0;                                // Cumulative mileage when tracing the trajectory using the following process
    const vector<Submap> &submaps = pcmap->submaps; // partial map
    const vector<Pose2D> &poses = pcmap->poses;     // robot trajectory
    double dmin = HUGE_VAL;                         // Minimum distance to last visited point
    size_t imin = 0, jmin = 0;                      // Index of previous visited point with minimum distance
    Pose2D prevP;                                   // Previous robot position
    for (size_t i = 0; i < submaps.size() - 1; i++)
    {                                      // Search for something other than the current partial map
        const Submap &submap = submaps[i]; // i-th partial map
        for (size_t j = submap.cntS; j <= submap.cntE;
             j++)
        {                        // About each robot position on the partial map
            Pose2D p = poses[j]; // robot position
            atdR += sqrt((p.tx - prevP.tx) * (p.tx - prevP.tx) +
                         (p.ty - prevP.ty) * (p.ty - prevP.ty));
            if (atd - atdR <
                atdthre)
            {                       // If the distance traveled to the current location is short, it will not be considered a loop and will stop.
                i = submaps.size(); // This will also get you out of the outer loop.
                break;
            }
            prevP = p;

            double d = (curPose.tx - p.tx) * (curPose.tx - p.tx) +
                       (curPose.ty - p.ty) * (curPose.ty - p.ty);
            if (d < dmin)
            { // Is the distance between the current position and p the smallest so far?
                dmin = d;
                imin = i; // Index of candidate partial map
                jmin = j; // Last visited point index
            }
            //      printf("i=%lu, j=%lu: atd=%g, atdR=%g, atdthre=%g\n", i, j, atd,
            //      atdR, atdthre);             // For confirmation
        }
    }

    printf("dmin=%g, radius=%g, imin=%lu, jmin=%lu\n", sqrt(dmin), radius, imin,
           jmin); // For confirmation

    if (dmin > radius * radius) // Loop detection is not possible if the distance to the last visited point is long.
        return (false);

    Submap &refSubmap =
        pcmap->submaps[imin]; // Make the nearest submap the reference scan
    const Pose2D &initPose = poses[jmin];
    printf("curPose:  tx=%g, ty=%g, th=%g\n", curPose.tx, curPose.ty, curPose.th);
    printf("initPose: tx=%g, ty=%g, th=%g\n", initPose.tx, initPose.ty,
           initPose.th);

    // Find the revisit point location
    Pose2D revisitPose;
    bool flag = estimateRevisitPose(curScan, refSubmap.mps, curPose, revisitPose);
    //  bool flag = estimateRelativePose(curScan, refSubmap.mps, initPose,
    //  revisitPose);

    if (flag)
    {                           // Loop detected
        Eigen::Matrix3d icpCov; // ICP covariance
        double ratio = pfu->calIcpCovariance(revisitPose, curScan,
                                             icpCov); // Calculate covariance of ICP

        LoopInfo info;                       // Loop detection result
        info.pose = revisitPose;             // Set revisit point position in loop arc information
        info.cov = icpCov;                   // Set covariance to loop arc information.
        info.curId = cnt;                    // Current position node id
        info.refId = static_cast<int>(jmin); // Node ID of last visited point
        makeLoopArc(info);                   // Loop arc generation

        // For confirmation
        Scan2D refScan;
        Pose2D spose = poses[refSubmap.cntS];
        refScan.setSid(info.refId);
        refScan.setLps(refSubmap.mps);
        refScan.setPose(spose);
        LoopMatch lm(*curScan, refScan, info);
        loopMatches.emplace_back(lm);
        printf("curId=%d, refId=%d\n", info.curId, info.refId);
    }

    return (flag);
}

//////////

// Generate a loop arc with the previous visited point (refId) as the start node and the current position (curId) as the end node.
void LoopDetectorSS::makeLoopArc(LoopInfo &info)
{
    if (info.arcked) // info's arc is already set
        return;
    info.setArcked(true);

    Pose2D srcPose = pcmap->poses[info.refId];                // Location of previous visited point
    Pose2D dstPose(info.pose.tx, info.pose.ty, info.pose.th); // Location of revisit point
    Pose2D relPose;
    Pose2D::calRelativePose(dstPose, srcPose, relPose); // Loop arc restraint

    // Since the arc constraint is a relative position from the start node, convert the covariance to the loop arc start node coordinate system.
    Eigen::Matrix3d cov;
    // Inverse covariance rotation
    CovarianceCalculator::rotateCovariance(srcPose, info.cov, cov, true);

    PoseArc *arc =
        pg->makeArc(info.refId, info.curId, relPose, cov); // Loop arc generation
    pg->addArc(arc);                                       // loop arc registration

    // 確認用
    printf("makeLoopArc: pose arc added\n");
    printf("srcPose: tx=%g, ty=%g, th=%g\n", srcPose.tx, srcPose.ty, srcPose.th);
    printf("dstPose: tx=%g, ty=%g, th=%g\n", dstPose.tx, dstPose.ty, dstPose.th);
    printf("relPose: tx=%g, ty=%g, th=%g\n", relPose.tx, relPose.ty, relPose.th);
    PoseNode *src = pg->findNode(info.refId);
    PoseNode *dst = pg->findNode(info.curId);
    Pose2D relPose2;
    Pose2D::calRelativePose(dst->pose, src->pose, relPose2);
    printf("relPose2: tx=%g, ty=%g, th=%g\n", relPose2.tx, relPose2.ty,
           relPose2.th);
}

//////////

// Perform ICP using the current scan curScan and the point cloud refLps of the partial map to find the position of the revisited point.
bool LoopDetectorSS::estimateRevisitPose(const Scan2D *curScan,
                                         const vector<LPoint2D> &refLps,
                                         const Pose2D &initPose,
                                         Pose2D &revisitPose)
{
    dass->setRefBase(refLps); // Set reference point cloud to data matcher
    cfunc->setEvlimit(0.2);   // Cost function error threshold

    printf("initPose: tx=%g, ty=%g, th=%g\n", initPose.tx, initPose.ty,
           initPose.th); // For confirmation

    size_t usedNumMin = 50;
    //  size_t usedNumMin = 100;

    // Thoroughly examine the area around the initial position initPose.
    // For efficiency, we do not perform ICP and simply check the matching score at each position.
    double rangeT = 1;  // Translational search range [m]
    double rangeA = 45; // Rotation search range [degrees]
    double dd = 0.2;    // Translational search interval [m]
    double da = 2;      // Rotation search interval [degrees]
    double pnrateMax = 0;
    vector<double> pnrates;
    double scoreMin = 1000;
    vector<double> scores;
    vector<Pose2D> candidates; // Candidate positions with good scores
    for (double dy = -rangeT; dy <= rangeT; dy += dd)
    {                                // Repeated search for translation y
        double y = initPose.ty + dy; // Add displacement dy to initial position
        for (double dx = -rangeT; dx <= rangeT; dx += dd)
        {                                // Repeated search for translation x
            double x = initPose.tx + dx; // Add displacement dx to initial position
            for (double dth = -rangeA; dth <= rangeA;
                 dth += da)
            { // Rotation search repetition
                double th =
                    MyUtil::add(initPose.th, dth); // Add displacement dth to initial position
                Pose2D pose(x, y, th);
                double mratio =
                    dass->findCorrespondence(curScan, pose); // Data correspondence by position pose
                size_t usedNum = dass->curLps.size();
                //        printf("usedNum=%lu, mratio=%g\n", usedNum, mratio); // 確認用
                if (usedNum < usedNumMin || mratio < 0.9) // Skip if response rate is poor
                    continue;
                cfunc->setPoints(dass->curLps, dass->refLps); // Set point cloud to cost function
                double score =
                    cfunc->calValue(x, y, th);      // Cost value (matching score)
                double pnrate = cfunc->getPnrate(); // Correspondence rate of detailed points
                //        printf("score=%g, pnrate=%g\n", score, pnrate); // For confirmation
                if (pnrate > 0.8)
                {
                    candidates.emplace_back(pose);
                    if (score < scoreMin)
                        scoreMin = score;
                    scores.push_back(score);
                    //          printf("pose: tx=%g, ty=%g, th=%g\n", pose.tx, pose.ty,
                    //          pose.th);  // For confirmation printf("score=%g, pnrate=%g\n",
                    //          score, pnrate);                    // For confirmation
                }
            }
        }
    }
    printf("candidates.size=%lu\n", candidates.size()); // For confirmation
    if (candidates.size() == 0)
        return (false);

    // Select the best position from among the candidate positions using ICP
    Pose2D best;                         // best candidate
    double smin = 1000000;               // ICP score minimum
    estim->setScanPair(curScan, refLps); // Scan settings for ICP
    for (size_t i = 0; i < candidates.size(); i++)
    {
        Pose2D p = candidates[i];        // Candidate position
        printf("score=%g\n", scores[i]); // For confirmation
        Pose2D estP;
        double score = estim->estimatePose(p, estP); // Find matching position with ICP
        double pnrate = estim->getPnrate();          // Correspondence rate of points in ICP
        size_t usedNum = estim->getUsedNum();        // Points used in ICP
        if (score < smin && pnrate >= 0.9 &&
            usedNum >= usedNumMin)
        { // Loop detection has strict conditions
            smin = score;
            best = estP;
            printf("smin=%g, pnrate=%g, usedNum=%lu\n", smin, pnrate,
                   usedNum); // For confirmation
        }
    }

    // Found if the minimum score is less than the threshold
    if (smin <= scthre)
    {
        revisitPose = best;
        return (true);
    }

    return (false);
}
