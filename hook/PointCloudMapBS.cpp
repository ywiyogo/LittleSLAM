﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file PointCloudMapBS.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "PointCloudMapBS.h"

using namespace std;

///////////

// Add robot position
void PointCloudMapBS::addPose(const Pose2D &p) { poses.emplace_back(p); }

// Add scan point cloud
void PointCloudMapBS::addPoints(const vector<LPoint2D> &lps)
{
    int skip = 5; // It's really heavy, so I thinned it out to 1/5.
                  //   int skip=10;
                  //   It's really heavy, so I thinned it out to 1/10.
    for (size_t i = 0; i < lps.size(); i += skip)
    {
        globalMap.emplace_back(lps[i]); // Just add it to the global map
    }
}

// Global map generation. It's already done so don't do anything
void PointCloudMapBS::makeGlobalMap()
{
    printf("globalMap.size=%lu\n", globalMap.size()); // For confirmation
}

// Local map generation. dummy
void PointCloudMapBS::makeLocalMap()
{
    //  localMap = globalMap;
}

////////

// dummy
void PointCloudMapBS::remakeMaps(const vector<Pose2D> &newPoses) {}
