﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file LoopDetector.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef LOOP_DETECTOR_H_
#define LOOP_DETECTOR_H_

#include "LPoint2D.h"
#include "MyUtil.h"
#include "Pose2D.h"
#include "PoseGraph.h"
#include "Scan2D.h"
#include <vector>

///////

// Loop arc setting information
struct LoopInfo
{
    bool arcked; // Have you already created a pose arc?
    int curId;   // Current keyframe id (scan)
    int refId;   // Reference keyframe id (scan or LocalGridMap2D)
    Pose2D
        pose;            // Global pose where the current keyframe matches the reference keyframe (or vice versa if Grid-based)
    double score;        // ICP matching score
    Eigen::Matrix3d cov; // covariance

    LoopInfo() : arcked(false), curId(-1), refId(-1), score(-1) {}

    ~LoopInfo() {}

    void setArcked(bool t) { arcked = t; }
};

//////////////

// Debug data
struct LoopMatch
{
    Scan2D curScan;
    Scan2D refScan;
    LoopInfo info;

    LoopMatch() {}

    LoopMatch(Scan2D &cs, Scan2D &rs, LoopInfo &i)
    {
        curScan = cs;
        refScan = rs;
        info = i;
    }
};


class LoopDetector
{
protected:
    PoseGraph *pg;                      // pose graph
    std::vector<LoopMatch> loopMatches; // For debugging

public:
    LoopDetector() {}

    ~LoopDetector() {}

    // For debugging
    std::vector<LoopMatch> &getLoopMatches() { return (loopMatches); }

    void setPoseGraph(PoseGraph *p) { pg = p; }

    virtual bool detectLoop(Scan2D *curScan, Pose2D &curPose, int cnt);
};

#endif
