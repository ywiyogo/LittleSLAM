﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file NNGridTable.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef _NN_GRID_TABLE_H_
#define _NN_GRID_TABLE_H_

#include "MyUtil.h"
#include "Pose2D.h"
#include <vector>

struct NNGridCell
{
    std::vector<const LPoint2D *> lps; // Scan point cloud stored in this cell

    void clear()
    {
        lps.clear(); // empty
    }
};

// lattice table
class NNGridTable
{
private:
    double csize;                  // Cell size [m]
    double rsize;                  // Size of target area [m]. half of one side of a square.
    int tsize;                     // half the table size
    std::vector<NNGridCell> table; // table body

public:
    NNGridTable() : csize(0.05), rsize(40)
    {                                               // Cell 5cm, target area 40x2m square
        tsize = static_cast<int>(rsize / csize);    // half the table size
        size_t w = static_cast<int>(2 * tsize + 1); // table size
        table.resize(w * w);                        // Secure area
        clear();                                    // Initialize table
    }

    ~NNGridTable() {}

    void clear()
    {
        for (size_t i = 0; i < table.size(); i++)
            table[i].clear(); // empty each cell
    }

    void addPoint(const LPoint2D *lp);
    const LPoint2D *findClosestPoint(const LPoint2D *clp, const Pose2D &predPose);
    void makeCellPoints(int nthre, std::vector<LPoint2D> &ps);
};

#endif
