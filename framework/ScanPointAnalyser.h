﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file ScanPointAnalyser.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef SCAN_ANALYSER_H_
#define SCAN_ANALYSER_H_

#include "LPoint2D.h"
#include "Scan2D.h"
#include <vector>

class ScanPointAnalyser
{
private:
    // Minimum distance to neighboring points [m]. If it is smaller than this, the error will be large, so do not use it for normal calculation.
    static const double FPDMIN;
    // Minimum distance to neighboring points [m]. If it is smaller than this, the error will be large, so do not use it for normal calculation.
    static const double FPDMAX;
    // Threshold for normal direction change [degrees]. If it is larger than this, it is considered a corner point.
    static const int CRTHRE = 45;
    static const int INVALID = -1;
    // Threshold for discrepancy between left and right normal directions
    double costh;

public:
    ScanPointAnalyser() : costh(cos(DEG2RAD(CRTHRE))) {}

    ~ScanPointAnalyser() {}

    //////////

    void analysePoints(std::vector<LPoint2D> &lps);
    bool calNormal(int idx, const std::vector<LPoint2D> &lps, int dir,
                   Vector2D &normal);
};

#endif
