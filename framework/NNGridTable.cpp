﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file NNGridTable.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "NNGridTable.h"

using namespace std;

////////////

// Register scan points lp in the grid table
void NNGridTable::addPoint(const LPoint2D *lp)
{
    // Index calculation for table search. First, check whether it is within the target area.
    int xi = static_cast<int>(lp->x / csize) + tsize;
    if (xi < 0 || xi > 2 * tsize) // outside the target area
        return;
    int yi = static_cast<int>(lp->y / csize) + tsize;
    if (yi < 0 || yi > 2 * tsize) // outside the target area
        return;

    size_t idx =
        static_cast<size_t>(yi * (2 * tsize + 1) + xi); // table index
    table[idx].lps.push_back(lp);                       // put it in the desired cell
}

///////////

// Find the closest point from the grid table to the position where scan point clp is coordinate-transformed using predPose
const LPoint2D *NNGridTable::findClosestPoint(const LPoint2D *clp,
                                              const Pose2D &predPose)
{
    LPoint2D glp;                    // predicted position of crp
    predPose.globalPoint(*clp, glp); // Coordinate transformation with relPose

    // clp table index. Check if it is within the target area.
    int cxi = static_cast<int>(glp.x / csize) + tsize;
    if (cxi < 0 || cxi > 2 * tsize)
        return (nullptr);
    int cyi = static_cast<int>(glp.y / csize) + tsize;
    if (cyi < 0 || cyi > 2 * tsize)
        return (nullptr);

    size_t pn = 0; // Total number of points in the searched cells. For confirmation
    double dmin = 1000000;
    const LPoint2D *lpmin = nullptr; // closest point (destination point)
    double dthre = 0.2;              // Exclude points further away than this [m]
    int R = static_cast<int>(dthre / csize);

    // Search for ±R squares
    for (int i = -R; i <= R; i++)
    {
        int yi = cyi + i; // spread from cyi
        if (yi < 0 || yi > 2 * tsize)
            continue;
        for (int j = -R; j <= R; j++)
        {
            int xi = cxi + j; // expand from cxi
            if (xi < 0 || xi > 2 * tsize)
                continue;

            size_t idx = yi * (2 * tsize + 1) + xi;   // table index
            NNGridCell &cell = table[idx];            // that cell
            vector<const LPoint2D *> &lps = cell.lps; // Scan point cloud of cell
            for (size_t k = 0; k < lps.size(); k++)
            {
                const LPoint2D *lp = lps[k];
                double d = (lp->x - glp.x) * (lp->x - glp.x) +
                           (lp->y - glp.y) * (lp->y - glp.y);

                if (d <= dthre * dthre &&
                    d < dmin)
                { // Save the point with minimum distance within dthre
                    dmin = d;
                    lpmin = lp;
                }
            }
            pn += lps.size();
        }
    }
    //  printf("pn=%d\n", pn);                 // Total number of points in the searched cells. For confirmation

    return (lpmin);
}

////////////

// Create a representative point for each cell in the grid table and store it in ps.
void NNGridTable::makeCellPoints(int nthre, vector<LPoint2D> &ps)
{
    // Currently, the scan numbers of each point within the cell are averaged.
    // If you want to take the latest value of the scan number, remove the comment from that part,
    // Comment out the line for taking the average (2 lines).

    size_t nn = 0; // Total number of cells in the table. For confirmation
    for (size_t i = 0; i < table.size(); i++)
    {
        vector<const LPoint2D *> &lps = table[i].lps; // Cell scan point cloud
        nn += lps.size();
        if (lps.size() >= nthre)
        {                          // Process only cells with points greater than nthre
            double gx = 0, gy = 0; // Center of gravity of point cloud
            double nx = 0, ny = 0; // Average normal vector of point cloud
            int sid = 0;
            for (size_t j = 0; j < lps.size(); j++)
            {
                const LPoint2D *lp = lps[j];
                gx += lp->x; // Accumulate position
                gy += lp->y;
                nx += lp->nx; // Accumulate normal vector components
                ny += lp->ny;
                sid += lp->sid; // When taking the average of scan numbers
                                //        if (lp->sid > sid)             // When taking the latest value of scan number
                                //          sid = lp->sid;
                                //        printf("sid=%d\n", lp->sid);
            }
            gx /= lps.size(); // average
            gy /= lps.size();
            double L = sqrt(nx * nx + ny * ny);
            nx /= L; // Average (normalized)
            ny /= L;
            sid /= lps.size(); // When taking the average of scan numbers

            LPoint2D newLp(sid, gx, gy); // Generate cell representative points
            newLp.setNormal(nx, ny);     // Normal vector settings
            newLp.setType(LINE);         // Make the type straight
            ps.emplace_back(newLp);      // add to ps
        }
    }

    //  printf("nn=%d\n", nn);               // Total number of cells in the table. For confirmation
}
