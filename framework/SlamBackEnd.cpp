﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file SlamBackEnd.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "SlamBackEnd.h"
#include "P2oDriver2D.h"

using namespace std;

////////// Pose Adjustment //////////

Pose2D SlamBackEnd::adjustPoses()
{
    //  pg->printArcs();
    //  pg->printNodes();

    newPoses.clear();

    P2oDriver2D p2o;
    p2o.doP2o(*pg, newPoses, 5); // Repeat 5 times

    return (newPoses.back());
}

/////////////////////////////

void SlamBackEnd::remakeMaps()
{
    // Modification of PoseGraph
    vector<PoseNode *> &pnodes = pg->nodes; // Pose Node
    for (size_t i = 0; i < newPoses.size(); i++)
    {
        Pose2D &npose = newPoses[i];
        PoseNode *pnode = pnodes[i]; // Nodes correspond 1:1 to robot position
        pnode->setPose(npose);       // Update the position of each node
    }
    printf("newPoses.size=%lu, nodes.size=%lu\n", newPoses.size(), pnodes.size());

    // Modification of PointCloudMap
    pcmap->remakeMaps(newPoses);
}
