﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file CostFunction.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef _COST_FUNCTION_H_
#define _COST_FUNCTION_H_

#include "LPoint2D.h"
#include "MyUtil.h"
#include "Pose2D.h"
#include "Scan2D.h"
#include <vector>

class CostFunction
{
protected:
    std::vector<const LPoint2D *> curLps; // Corresponding point cloud of current scan
    std::vector<const LPoint2D *> refLps; // Point cloud of matched reference scans
    double evlimit;                       // Distance threshold for determining correspondence in matching
    double pnrate;                        // Ratio of points with matching errors within evlimit

public:
    CostFunction() : evlimit(0), pnrate(0) {}

    ~CostFunction() {}

    void setEvlimit(double e) { evlimit = e; }

    // Set corresponding point cloud cur, ref with DataAssociator
    void setPoints(std::vector<const LPoint2D *> &cur,
                   std::vector<const LPoint2D *> &ref)
    {
        curLps = cur;
        refLps = ref;
    }

    double getPnrate() { return (pnrate); }

    virtual double calValue(double tx, double ty, double th) = 0;
};

#endif
