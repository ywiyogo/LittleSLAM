﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file ScanMatcher2D.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef SCAN_MATCHER2D_H_
#define SCAN_MATCHER2D_H_

#include "LPoint2D.h"
#include "MyUtil.h"
#include "PointCloudMap.h"
#include "Pose2D.h"
#include "PoseEstimatorICP.h"
#include "PoseFuser.h"
#include "RefScanMaker.h"
#include "Scan2D.h"
#include "ScanPointAnalyser.h"
#include "ScanPointResampler.h"
#include <vector>

// Perform scan matching using ICP
class ScanMatcher2D
{
private:
    int cnt;         // Logical time. Corresponds to scan number
    Scan2D prevScan; // Previous scan
    Pose2D initPose; // Location of the map origin. Normal(0,0,0)

    double scthre; // Score threshold. If it is larger than this, it is considered
                   // an ICP failure.
    double nthre;  // Usage points threshold. If it is smaller than this, it is
                   // considered an ICP failure.
    double atd;    // Cumulative mileage. For confirmation
    bool dgcheck;  // Do degeneration processing?

    PoseEstimatorICP *estim;   // robot position estimator
    PointCloudMap *pcmap;      // point cloud map
    ScanPointResampler *spres; // Uniform scan point spacing
    ScanPointAnalyser *spana;  // Scan point normal calculation
    RefScanMaker *rsm;         // Generate reference scan
    PoseFuser *pfu;            // sensor fusion device
    Eigen::Matrix3d cov;       // Covariance matrix of robot movement amount
    Eigen::Matrix3d totalCov;  // covariance matrix of robot position

    std::vector<PoseCov> poseCovs; // for debugging

public:
    ScanMatcher2D()
        : cnt(-1), scthre(1.0), nthre(50), dgcheck(false), atd(0), pcmap(nullptr),
          spres(nullptr), spana(nullptr), estim(nullptr), rsm(nullptr),
          pfu(nullptr) {}

    ~ScanMatcher2D() {}

    /////// Framework modifications ////////

    void setPoseEstimator(PoseEstimatorICP *p) { estim = p; }

    void setPoseFuser(PoseFuser *p) { pfu = p; }

    void setScanPointResampler(ScanPointResampler *s) { spres = s; }

    void setScanPointAnalyser(ScanPointAnalyser *s) { spana = s; }

    void setRefScanMaker(RefScanMaker *r)
    {
        rsm = r;
        if (pcmap != nullptr)
            rsm->setPointCloudMap(pcmap);
    }

    void setPointCloudMap(PointCloudMap *m)
    {
        pcmap = m;
        if (rsm != nullptr)
            rsm->setPointCloudMap(pcmap);
    }

    ///////

    void reset() { cnt = -1; }

    void setInitPose(Pose2D &p) { initPose = p; }

    void setDgCheck(bool t) { dgcheck = t; }

    Eigen::Matrix3d &getCovariance() { return (cov); }

    // デバッグ用
    std::vector<PoseCov> &getPoseCovs() { return (poseCovs); }

    //////////

    bool matchScan(Scan2D &scan);
    void growMap(const Scan2D &scan, const Pose2D &pose);
};

#endif
