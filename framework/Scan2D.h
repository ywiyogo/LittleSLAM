﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file Scan2D.h
 * @author Masahiro Tomono
 ****************************************************************************/

#ifndef SCAN2D_H_
#define SCAN2D_H_

#include "LPoint2D.h"
#include "MyUtil.h"
#include "Pose2D.h"
#include <vector>

//////////

// scan
struct Scan2D
{
    static double MAX_SCAN_RANGE; // Scan point distance value upper limit [m]
    static double MIN_SCAN_RANGE; // Scan point distance value lower limit [m]

    int sid;                   // scan id
    Pose2D pose;               // Odometric values at the time of scan acquisition
    std::vector<LPoint2D> lps; // scan point cloud

    Scan2D() : sid(0) {}

    ~Scan2D() {}

    ///////

    void setSid(int s) { sid = s; }

    void setLps(const std::vector<LPoint2D> &ps) { lps = ps; }

    void setPose(Pose2D &p) { pose = p; }
};

#endif
