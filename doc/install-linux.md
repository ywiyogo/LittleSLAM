﻿## How to use LittleSLAM (on Linux)

### (1) Installation of related software

- C++ compiler (gcc), Boost, Eigen, CMake, gnuplot  
The following commands are used to install them all together.

</code></pre>
<pre><code> $ sudo apt-get install build-essential cmake libboost-all-dev libeigen3-dev gnuplot gnuplot-x11
</code></pre>

- p2o  
[p2o](https://github.com/furo-org/p2o)Open the Github site for Download p2o by either of the following methods  
(A) Click the "Clone or download" button on the Github page and select "Download ZIP",
The method of extracting the zip file is described later.  
(B) Clone the repository using git.

### (2) LittleSLAM installation

- LittleSLAM Deployment  
[LittleSLAM](https://github.com/furo-org/LittleSLAM)Open the Github site for
Download LittleSLAM by either of the following methods  
(A) Click the "Clone or download" button on the Github screen and select "Download ZIP",
Download LittleSLAM-master.zip.
Then, extract the zip file to an appropriate directory.
For example, let's say you want to extract it to "\~/LittleSLAM".
Under the "LittleSLAM-master" directory in LittleSLAM-master.zip
Copy the 4 directories and 3 files under the "LittleSLAM-master" directory in "LittleSLAM-master.zip" to "\~/LittleSLAM".  
(B) Clone the repository using git.

```
git clone --recursive https://github.com/furo-org/LittleSLAM.git
```

If you use this method, the following "p2o expansion" operation is not necessary.

- p2o Deployment
Copy the file "p2o.h" in the aforementioned p2o-master.zip to "\~/LittleSLAM/p2o".

- Create build directory
Create a build directory under  "\~/LittleSLAM"
The directory structure up to this point is as follows

![directory structure](images/folders-lnx.png)

- Running CMake  
In the console, go to the build directory and run cmake.

```
$ cd build && cmake ..
```

The figure below shows an example of cmake execution.

![cmake](images/cmake-lnx.png)

Alternatively, you can install the GUI version of CMake and run CMake in the GUI as you would on Windows.
You can also run CMake in the GUI.

-Build  
In the console, run make in the build directory.
```
~/LittleSLAM/build$ make
```
If the build is successful, an executable file LittleSLAM is created in the directory "\~/LittleSLAM/build/cui".  

![cmake](images/exefile-lnx.png)

### (3) Execution

The following command executes LittleSLAM.

```
./LittleSLAM [-so] Data file name [start scan number].
```

The -s option draws one scan at a time. Use this option when you want to check each scan shape.
to check the geometry of each scan.  
The -o option generates a map (not a SLAM map) of scans lined up with odometry data.
If no options are specified, SLAM will be executed.
If a start scan number is specified, the scan will be skipped to that number before being executed.

As an example, the following command executes SLAM.  
In this example, the data file "corridor.lsc" is located in the directory "\~/LittleSLAM/dataset".

```
~/LittleSLAM/build/cui$ ./LittleSLAM ~/LittleSLAM/dataset/corridor.lsc
```

![cmake](images/command-lnx.png)  
  
When you run the command, LittleSLAM reads data from the file and builds the map piece by piece.
We will continue to build it. The situation will be drawn in gnuplot.
Finally, a map like the one shown below will be generated.
Even after the process is finished, the program does not exit and the map remains displayed.
Press Ctrl-C to exit the program.

![cmake](images/result-lnx.png)
