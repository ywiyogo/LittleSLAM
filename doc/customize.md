﻿## Customize your program

LittleSLAM is largely composed of the following elemental technologies: scan matching, sensor fusion, and loop closure.
LittleSLAM is largely composed of the following elemental technologies: scan matching, sensor fusion, and loop closure.
LittleSLAM is designed to allow for some customization of these techniques as a learning program.
LittleSLAM is designed to allow for some customization of these techniques as a learning program.
The table below shows the types of customization available.
For details on each, please refer to the reference book [1].

| Type | Content |
|:--------------------|:-------------|
| customizeA | scan matching basic type |
| customizeB | Scan Matching Improved Form 1 |
customizeC | Scan Matching Improved Form 2 | customizeD | Scan Matching Improved Form 2 | customizeC | Scan Matching Improved Form 3
customizeD | scan matching improved form3 | customizeE | scan matching improved form4 | customizeC | scan matching improved form5 | customizeC | scan matching improved form6
customizeE | scan matching improved form4 | customizeF | scan matching improved form5 | customizeC | scan matching improved form6 | customizeC | scan matching improved form7
customizeF | scan matching improved5 | customizeG | scan matching improved6 | customizeC | scan matching improved7 | customizeD | scan matching improved8
customizeG | improved scan matching6 | customizeH | improved sensor fusion6 | customizeD | improved scan matching7 | customizeE | improved scan matching8
customizeH | Dealing with Degeneration due to Sensor Fusion | customizeI | Loop Closure
| customizeI | loop closure

The type of customization is determined by the
The type of customization is specified in the function customizeFramework in SlamLauncher.cpp as follows.
The type in the table is the function name as it is, and write the function you want to specify,
The other functions should be commented out.  
The default is customizeI.

```C++

void SlamLauncher::customizeFramework() {
  fcustom.setSlamFrontEnd(&sfront);
  fcustom.makeFramework();
  // fcustom.customizeG(); // commented out since not used
  fcustom.customizeI(); // specify this customization

  pcmap = fcustom.getPointCloudMap();
}

````  

The function customizeX (X=A to I) is also defined in cui/FrameworkCustomizer.cpp.  
Users can also create a new customizeX and try it out.
