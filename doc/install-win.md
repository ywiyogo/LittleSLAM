﻿## How to use LittleSLAM (for Windows)

### (1) Installing related software

- Boost
Download [Boost](http://www.boost.org/) and unzip it to an appropriate folder.
LiitleSLAM uses only Boost header files, so no build is required.

-Eigen3
[Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page)
Download it and unzip it to a suitable folder.
Eigen is a library that is used only with header files, so no building is required.

-gnuplot
Download and install [gnuplot](http://www.gnuplot.info/).
From LittleSLAM, it is called with an execution command rather than an API, so
Set the gnuplot path in the Windows environment variable Path.
For example, if you installed gnuplot in the folder C:\gnuplot, use "Path=... ;C:\gnuplot\bin; ...".
(The installer may set it automatically)

-CMake
Download and install [CMake](https://cmake.org/).

-p2o
Open the Github site for [p2o](https://github.com/furo-org/p2o). Download p2o using one of the following methods.
(A) Press the "Clone or download" button on the Github screen, select "Download ZIP",
Download p2o-master.zip. The method for extracting the zip file will be explained later.
(B) Clone the repository using git.

### (2) Installing LittleSLAM

- Deploying LittleSLAM
Open the Github site for [LittleSLAM](https://github.com/furo-org/LittleSLAM).
Download LittleSLAM using one of the following methods.
(A) Press the "Clone or download" button on the Github screen, select "Download ZIP",
Download LittleSLAM-master.zip.
Then extract this zip file to a suitable folder.
For example, let's say you want to expand it to "C:\abc\LittleSLAM".
"abc" is an arbitrary folder determined by the user.
Under the "LittleSLAM-master" folder in LittleSLAM-master.zip
Copy 4 folders and 3 files under "C:\abc\LittleSLAM".
(B) Clone the repository using git.

````
git clone --recursive https://github.com/furo-org/LittleSLAM.git
````

If you use this method, the next step of "p2o expansion" is not necessary.

- p2o deployment
Copy the file "p2o.h" in the p2o-master.zip mentioned above to "C:\abc\LittleSLAM\p2o".

-Create build folder
Create a build folder under "C:\abc\LittleSLAM".
The folder structure so far is as follows.
![フォルダ構成](images/folders.png)

- Run CMake
Run CMake (GUI) to generate LittleSLAM.sln.
First, specify the folder shown below in the "Where is the source code" and "Where to buid the binaries" fields.
Then press the Configure button.
When you run CMake for LittleSLAM for the first time, you will be asked for a C++ compiler as shown below, so
Specify the C++ compiler you are using, select "Use default native compliers", and press the Finish button.
Then press the Configure button again and finally the Generate button.

![cmake](images/cmake.png)

- Specification of Eigen3
If CMake cannot find the location of Eigen3 (EIGEN3_INCLUDE_DIR) and an error occurs,
Restart CMake and retry Configure and Generate by doing one of the following:
(A) Add EIGEN3_ROOT_DIR to the Windows system environment variables,
Set the folder where Eigen3 was extracted.
Then, as shown in the figure below, in each CMakeLists.txt of cui, framework, and hook under "C:\abc\LittleSLAM",
The folder specified by EIGEN3_ROOT_DIR is set to EIGEN3_INCLUDE_DIR.
(B) Manually set the Eigen3 folder for each CMakeLists.txt.
For example, if Eigen3 is expanded to "C:\eigen", the image below is displayed.
Rewrite $ENV{EIGEN3_ROOT_DIR} to C:\eigen.

```
-- Excerpt from CMakeLists.txt --

find_package(Eigen3)  
IF(NOT EIGEN3_INCLUDE_DIR)          # Eigen3 path not found
  set(EIGEN3_INCLUDE_DIR $ENV{EIGEN3_ROOT_DIR})
ENDIF() 
```  

-Start Visual studio
LittleSLAM.sln has been created under "C:\abc\LittleSLAM\build", so
Double click on it and Visual studio will start.

- Build
In Visual studio, specify Release, x64 (for 64-bit) and run Build Solution from the Build menu, as shown in the image below.

![cmake](images/build.png)


If the build is successful, the executable file LittleSLAM.exe will be generated in the "build\cui\Release" folder. 

![cmake](images/exefile.png)

### (3) Execution

Run LittleSLAM with the following command from the Windows command prompt.

</code></pre>
<pre><code> LittleSLAM [-so] Data file name [Start scan number]
</code></pre>

If the -s option is specified, each scan will be drawn one by one. When you want to check each scan shape
Use.
With the -o option, a map of scans arranged by odometry data is displayed.
(not a SLAM map).
If no options are specified, SLAM will be executed.
If you specify a starting scan number, the scan will be skipped up to that number and then executed.

As an example, run SLAM with the command below.
In this example, a data file called "corridor.lsc" is placed in the "C:\abc\dataset" folder.

</code></pre>
<pre><code> C:\abc\LittleSLAM\build\cui\Release> LittleSLAM C:\abc\dataset\corridor.lsc
</code></pre>

![cmake](images/command.png)  
  
When you run the command, LittleSLAM reads data from the file and builds the map piece by piece.
We will continue to build it. The situation will be drawn in gnuplot.
Finally, a map like the one shown below will be generated.
Even after the process is finished, the program does not exit and the map remains displayed.
Press Ctrl-C to exit the program.

![cmake](images/result.png)
