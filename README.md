﻿# LittleSLAM

LittleSLAM is a SLAM learning program.
Input the file containing the 2D laser scanner data (scan) and odometry data,
Output the robot position trajectory and 2D point cloud map on gnuplot.

LittleSLAM performs alignment based on scan matching, sensor fusion of laser scanner and odometry,
It consists of elemental technologies such as loop closure based on Graph-based SLAM.

LittleSLAM is a program created as a teaching material for the reference book [1].
We use a simple algorithm to prioritize ease of understanding.
Therefore, the performance will be lower compared to a full-spec SLAM program, but
The content is easy to understand.

## Execution environment

LittleSLAM is written in the programming language C++.
The execution environments for which operation has been confirmed are as follows. Both are 64-bit versions.

| OS | C++ |
|:--:|:---:|
| Windows 7 | Visual C++ 2013 (Visual Studio Community 2013)|
| Windows 10 | Visual C++ 2015 (Visual Studio Community 2015)|
| Linux Ubuntu 14.04 LTS | gcc 4.8.4|
| Linux Ubuntu 16.04 LTS | gcc 5.4.0|

I have not tested it on 32-bit OS, so please try it yourself if you need it.。

## Required software

The following software is required to run LittleSLAM.

| Software | Contents | Version |
|:------------:|:----:|:----------:|
| Boost        | C++ general purpose library |1.58.0 |
| Eigen3       | linear algebra library|3.2.4 |
| gnuplot      | graph drawing tools  |5.0 |
| CMake        | Build support tools |3.2.2 |
| p2o          | Graph-based SLAM solver|beta |

The version is the one used in the development of LittleSLAM and is not a clear condition.
Any version higher than this will usually work.
It may also work with lower versions.

## How to use

- How to use it on Windows[here](doc/install-win.md)

- How to use it on Linux[here](doc/install-linux.md)

## Dataset

We have prepared 6 data files for the experiment. A list is shown in the table below.
It can be downloaded from [here](https://furo.org/software/little_slam/dataset.zip).

| File name | Contents |
|:--------------------|:-------------|
| corridor.lsc        | Corridor (single loop)|
| hall.lsc            | Hall (single loop) |
| corridor-degene.lsc | Corridor (degenerate) |
| hall-degene.lsc     | Hall (degenerate) |
| corridor-loops.lsc  | Corridor (multiple loops) |
| hall-loops.lsc      | Hall (multiple loops) |

## customization

LittleSLAM is a learning program that has undergone several improvements from its basic form.
You can customize it to make it perfect.
Please refer to [details](doc/customize.md).

## Reference books

The following books are SLAM manuals. In addition to giving a general explanation of SLAM,
As a specific example, we use LittleSLAM as a teaching material and explain the details of its source code.

[1] Masahiro Tomono, "Introduction to SLAM -- Robot self-position estimation and map construction technology", Ohmsha, 2018

## License

- LittleSLAM is based on the MPL-2.0 license.
