﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file FrameworkCustomizer.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "FrameworkCustomizer.h"

using namespace std;

// Set up the basics of the framework
void FrameworkCustomizer::makeFramework()
{
    smat.setPoseEstimator(&poest);
    smat.setPoseFuser(&pfu);

    // The parts to be used for LoopDetectorSS have been decided, so insert them here.
    lpdSS.setPoseEstimator(&poest);
    lpdSS.setPoseFuser(&pfu);
    lpdSS.setDataAssociator(&dassGT);
    lpdSS.setCostFunction(&cfuncPD);
    lpdSS.setPointCloudMap(&pcmapLP);

    sfront->setScanMatcher(&smat);
}

/////// 実験用

// Framework basic configuration
void FrameworkCustomizer::customizeA()
{
    pcmap = &pcmapBS;               // Point cloud map that stores all scan points
    RefScanMaker *rsm = &rsmBS;     // Set the previous scan as the reference scan
    DataAssociator *dass = &dassLS; // Data matching using linear search
    CostFunction *cfunc = &cfuncED; // Let Euclidean distance be the cost function
    PoseOptimizer *popt = &poptSD;  // Optimization using steepest descent method
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Use of lattice table
void FrameworkCustomizer::customizeB()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassLS; // Data matching using linear search
    CostFunction *cfunc = &cfuncED; // Let Euclidean distance be the cost function
    PoseOptimizer *popt = &poptSD;  // Optimization using steepest descent method
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Perform straight line search after steepest descent
void FrameworkCustomizer::customizeC()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassLS; // Data matching using linear search
    CostFunction *cfunc = &cfuncED; // Let Euclidean distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Perform data correspondence using a lattice table
void FrameworkCustomizer::customizeD()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncED; // Let Euclidean distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Added equalization of scan point spacing
void FrameworkCustomizer::customizeE()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncED; // Let Euclidean distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    smat.setScanPointResampler(&spres); // Equalization of scan point spacing
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Added normal calculation for scan points
void FrameworkCustomizer::customizeF()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncPD; // Let vertical distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    smat.setScanPointAnalyser(&spana); // Scan point normal calculation
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Added scan point interval equalization and normal calculation
void FrameworkCustomizer::customizeG()
{
    pcmap = &pcmapGT;               // Point cloud map managed with grid table
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncPD; // Let vertical distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    smat.setScanPointResampler(&spres); // Equalization of scan point spacing
    smat.setScanPointAnalyser(&spana);  // Scan point normal calculation
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(false); // Sensor not fused
}

// Added sensor fusion
void FrameworkCustomizer::customizeH()
{
    //  pcmap = &pcmapGT;                                //
    //  Point cloud map managed with grid table
    pcmap = &pcmapLP;               // A point cloud map managed for each partial map. For comparison with cI
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncPD; // Let vertical distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdDM;     // Dummy loop detection

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    smat.setScanPointResampler(&spres);
    smat.setScanPointAnalyser(&spana);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(true); // sensor fusion
}

// Added sensor fusion and loop closure
void FrameworkCustomizer::customizeI()
{
    pcmap = &pcmapLP;               // Point cloud map managed for each partial map
    RefScanMaker *rsm = &rsmLM;     // Use local map as reference scan
    DataAssociator *dass = &dassGT; // Data mapping using lattice table
    CostFunction *cfunc = &cfuncPD; // Let vertical distance be the cost function
    PoseOptimizer *popt = &poptSL;  // Optimization using steepest descent method and line search
    LoopDetector *lpd = &lpdSS;     // Loop detection using partial maps

    popt->setCostFunction(cfunc);
    poest.setDataAssociator(dass);
    poest.setPoseOptimizer(popt);
    pfu.setDataAssociator(dass);
    smat.setPointCloudMap(pcmap);
    smat.setRefScanMaker(rsm);
    smat.setScanPointResampler(&spres);
    smat.setScanPointAnalyser(&spana);
    sfront->setLoopDetector(lpd);
    sfront->setPointCloudMap(pcmap);
    sfront->setDgCheck(true); // sensor fusion
}
