﻿/****************************************************************************
 * LittleSLAM: 2D-Laser SLAM for educational use
 * Copyright (C) 2017-2018 Masahiro Tomono
 * Copyright (C) 2018 Future Robotics Technology Center (fuRo),
 *                    Chiba Institute of Technology.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * @file SlamLauncher.cpp
 * @author Masahiro Tomono
 ****************************************************************************/

#include "SlamLauncher.h"
#include "ScanPointResampler.h"
#include <boost/algorithm/string/predicate.hpp>
#include <boost/timer.hpp>

using namespace std; // Use C++ Standard Library namespaces

//////////

void SlamLauncher::run()
{
    mdrawer.initGnuplot(); // gnuplot initialization
    mdrawer.setAspectRatio(
        -0.9); // Ratio of x-axis to y-axis (if negative, content is constant)

    size_t cnt = 0; // Logical time of processing
    if (startN > 0)
        skipData(startN); // skip data to startN

    double totalTime = 0, totalTimeDraw = 0, totalTimeRead = 0;
    Scan2D scan;
    bool eof = sreader.loadScan(cnt, scan); // Read one scan from file
    boost::timer tim;
    while (!eof)
    {
        if (odometryOnly)
        { // Map construction by odometry (preferred over SLAM)
            if (cnt == 0)
            {
                ipose = scan.pose;
                ipose.calRmat();
            }
            mapByOdometry(&scan);
        }
        else
            sfront.process(scan); // Map construction by SLAM

        double t1 = 1000 * tim.elapsed();

        if (cnt % drawSkip == 0)
        { // drawSkipおきに結果を描画
            mdrawer.drawMapGp(*pcmap);
        }
        double t2 = 1000 * tim.elapsed();

        ++cnt;                             // Logical time update
        eof = sreader.loadScan(cnt, scan); // Load next scan

        double t3 = 1000 * tim.elapsed();
        totalTime = t3;             // Total Processing Time
        totalTimeDraw += (t2 - t1); // Total drawing time
        totalTimeRead += (t3 - t2); // Total load time

        printf("---- SlamLauncher: cnt=%lu ends ----\n", cnt);
    }
    sreader.closeScanFile();

    printf("Elapsed time: mapping=%g, drawing=%g, reading=%g\n",
           (totalTime - totalTimeDraw - totalTimeRead), totalTimeDraw,
           totalTimeRead);
    printf("SlamLauncher finished.\n");

    // To keep the drawing screen after the process is finished, use sleep to make
    // an infinite loop. ctrl-C to exit.
    while (true)
    {
#ifdef _WIN32
        Sleep(1000); // Sleep on Windows
#elif __linux__
        usleep(1000000); // Sleep on Linux
#endif
    }
}

// Skip reading from start to num scans
void SlamLauncher::skipData(int num)
{
    Scan2D scan;
    bool eof = sreader.loadScan(0, scan);
    for (int i = 0; !eof && i < num; i++)
    { // Read num blanks
        eof = sreader.loadScan(0, scan);
    }
}

///////// Map construction by odometry //////////

void SlamLauncher::mapByOdometry(Scan2D *scan)
{
    //  Pose2D &pose = scan->pose;               // Odometry position at the time
    //  of scan acquisition
    Pose2D pose;
    Pose2D::calRelativePose(scan->pose, ipose, pose);
    vector<LPoint2D> &lps = scan->lps; // scan point cloud
    std::vector<LPoint2D> glps;        // Point cloud in map coordinate system
    for (size_t j = 0; j < lps.size(); j++)
    {
        LPoint2D &lp = lps[j];
        LPoint2D glp;
        pose.globalPoint(
            lp,
            glp); // Convert from sensor coordinate system to map coordinate system
        glps.emplace_back(glp);
    }

    // Store data in point cloud map pcmap
    pcmap->addPose(pose);
    pcmap->addPoints(glps);
    pcmap->makeGlobalMap();

    printf("Odom pose: tx=%g, ty=%g, th=%g\n", pose.tx, pose.ty, pose.th);
}

////////// scan drawing ////////

void SlamLauncher::showScans()
{
    mdrawer.initGnuplot();
    mdrawer.setRange(6); // Drawing range. If the scan is 6m square
    mdrawer.setAspectRatio(
        -0.9); // Ratio of x-axis to y-axis (if negative, content is constant)

    ScanPointResampler spres;

    size_t cnt = 0; // 処理の論理時刻
    if (startN > 0)
        skipData(startN); // startNまでデータを読み飛ばす

    Scan2D scan;
    bool eof = sreader.loadScan(cnt, scan);
    while (!eof)
    {
        //    spres.resamplePoints(&scan);         //
        //    コメントアウトはずせば、スキャン点間隔を均一にする。

        // 描画間隔をあける
#ifdef _WIN32
        Sleep(100); // WindowsではSleep
#elif __linux__
        usleep(100000);  // Linuxではusleep
#endif

        mdrawer.drawScanGp(scan); // スキャン描画

        printf("---- scan num=%lu ----\n", cnt);
        eof = sreader.loadScan(cnt, scan);
        ++cnt;
    }
    sreader.closeScanFile();
    printf("SlamLauncher finished.\n");
}

//////// スキャン読み込み /////////

bool SlamLauncher::setFilename(char *filename)
{
    bool flag = sreader.openScanFile(filename); // ファイルをオープン

    return (flag);
}

////////////

void SlamLauncher::customizeFramework()
{
    fcustom.setSlamFrontEnd(&sfront);
    fcustom.makeFramework();
    //  fcustom.customizeG();                         // do not deal with
    //  degeneration fcustom.customizeH();                         // deal with
    //  degeneration
    fcustom.customizeI(); // close the loop

    pcmap = fcustom.getPointCloudMap(); // What to do after customize
}
